# Docker Architecture

![](https://bitbucket.org/budash_mobwizards/dockerapp/raw/f1e89f60330ebfe6706f97539c9436917a9dd7ac/static/initial-docker.png)

## Description
- Our docker architecture consists of two containers which is connected by common docker network. The containers are : 

	1. ** php-apache container**
	This container acts as our server. It has php 7.4, apache2 , composers preinstalled. Its image is build using custom Dockerfile.

	2. **mysql container**
	This container acts as out database and comes with preinstalled MYSQL 8, host and server client

- Lastly, both above container are connected via docker network : **wizards**

## Requirements
Make sure you have installed both :

1. Docker
2. Docker Compose :  https://docs.docker.com/compose/install/

## Installation [ One time setup ]
1. Clone the repository  
2. Change directory to repository . ie  'cd dockerapp'. If you have clonned repo before run ``` git pull ``` first.
3. Inside you will find 'docker-compose.yml' file, which will be used to start and      stop the containers, create and destroy network.
4. Run command ``` docker volume ls ``` to check volumes. 
	- If there exists a volume named **docker_dbdata** , delete it as  we will be creating volume of same name for mysql  later on. This needs to be done only the first time.
	- Delete volume command ``` docker volume rm docker_dbdata```
5. Run command  ``` docker-compose up -d```
	- If you are running it the first time, it will builds and saves our custom php-apache image and mysql 8 image. It might take some time ~5 - 10 minutes to complete.
	- After building images successfully , it start the containers which can be view by running command ``` docker ps ```. The -d flag runs the process in background (detached mode).
6. Load wzdstosa_logging database schema into mysql container by running command below
	- ``` docker exec -i <db_container_id> mysql [--user yourusername] [--password=yourpassword] databasename < /path/to/db.dump ```
	
7. Terminating the containers using command ``` docker-compose down ``` 
	- Terminates the containers and network.


### Build updated php-apache image after change
1. cd into dockerapp,  Pull the dockerapp repo.
2. Change directory to “ dockerapp/php-image ” ie cd dockerapp/php-image.
3.  We are going to update and build our mobwizards/php-apache image. Run command below
inside of directory dockerapp/php-apache: 
	
	``` docker build -t mobwizards/php-apache:latest . ```

4. Yes that ‘.’ at the end is also needed. After it completes, run command below to start our container 

	``` docker-compose up -d ```

5. if you had previously running containers stop them ```  docker-compose down ```



### Note: 
- reload apache without terminationg container:  /etc/init.d/apache2 reload
